[@bs.module "react"] [@bs.scope "Children"] [@bs.val]
external count: React.element => int = "count";
[@bs.module "react"] [@bs.scope "Children"] [@bs.val]
external toArrayOfElements: React.element => array(React.element) = "toArray";
[@bs.module "react"] [@bs.scope "Children"] [@bs.val]
external toArrayOfObjects: React.element => array(Js.t('a)) = "toArray";