exception GetError(string);

type country = {
  label: string,
  value: string,
};

let countries_url = "https://gist.githubusercontent.com/rusty-key/659db3f4566df459bd59c8a53dc9f71f/raw/4127f9550ef063121c564025f6d27dceeb279623/counties.json";

module Decode = {
  let country = json =>
    Json.Decode.{
      label: json |> field("label", string),
      value: json |> field("value", string),
    };

  let countries = json => Json.Decode.array(country, json);
};

let defaultError = "Unable to fetch countries";

let sanitizeResponse = response => {
  if (!Fetch.Response.ok(response)) {
    let error = Fetch.Response.statusText(response);
    raise(GetError(error));
  };

  response;
};

let getCountries = () => {
  Js.Promise.(
    Fetch.fetch(countries_url)
    |> then_(response =>
         try (sanitizeResponse(response) |> Fetch.Response.json) {
         | GetError(error) => reject(GetError(error))
         }
       )
    |> then_(json => json |> Decode.countries |> resolve)
  );
};

let handlePromiseFailure =
  [@bs.open]
  (
    fun
    | GetError(err) => {
        err;
      }
  );