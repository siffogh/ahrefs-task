[%bs.raw {|require('./App.css')|}];

[@react.component]
let make = () => {
  let (country, setCountry) = React.useState(_ => Some("us"));
  <div className="App">
    <h1> {"Country Select" |> ReasonReact.string} </h1>
    <CountrySelect
      className="ahrefs country-select"
      country
      onChange={newCountry => setCountry(_ => Some(newCountry))}
    />
  </div>;
};