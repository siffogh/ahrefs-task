let countryOptionRenderer = props => {
  <CountrySelect_Option props>
    <span className={"flag-icon flag-icon-" ++ props##data##value} />
    {ReasonReact.string(props##data##label)}
  </CountrySelect_Option>;
};

let menuRenderer = props => {
  let childrenElements = Children.toArrayOfElements(props##children);
  let childrenObjects = Children.toArrayOfObjects(props##children);
  let selectedChildIndex =
    childrenObjects
    |> Js.Array.findIndex(childObject => childObject##props##isSelected);
  let focusedChildIndex =
    childrenObjects
    |> Js.Array.findIndex(childObject => childObject##props##isFocused);

  let scrollToIndex =
    switch (selectedChildIndex, focusedChildIndex) {
    | (someSelectedChildIndex, (-1)) when someSelectedChildIndex !== (-1) => someSelectedChildIndex
    | _ => focusedChildIndex
    };

  let rowRenderer = rowProps => {
    let child = childrenElements[rowProps##index];
    <div
      key={
        rowProps##key;
      }
      style={
        rowProps##style;
      }> child </div>;
  };

  <CountrySelect_List
    className="country-select-list"
    width=400
    height=300
    rowCount={childrenElements->Js.Array.length}
    rowHeight=31
    rowRenderer
    scrollToIndex
  />;
};