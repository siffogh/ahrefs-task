[%bs.raw {|require('flag-icon-css/css/flag-icon.css')|}];
[%bs.raw {|require('./country-select.css')|}];

[@bs.module] external caret: string = "./icons/caret-down.svg";

let combineClassNames = (baseClassName, className) => {
  switch (className) {
  | None => baseClassName
  | Some(definedClassname) => baseClassName ++ " " ++ definedClassname
  };
};

let countryToOption = country => {
  "label": country.CountriesAPI.label,
  "value": country.value,
};

type state = {
  countries: array(CountriesAPI.country),
  isLoading: bool,
  menuIsOpen: bool,
};

type action =
  | SET_COUNTRIES(array(CountriesAPI.country))
  | SET_MENU_IS_OPEN(bool)
  | TOGGLE_MENU_IS_OPEN;

[@react.component]
let make = (~className=?, ~country: option(string), ~onChange=?) => {
  let initialState = {countries: [||], isLoading: false, menuIsOpen: false};

  let reducer = (state, action) => {
    switch (action) {
    | SET_COUNTRIES(countries) => {...state, isLoading: false, countries}
    | SET_MENU_IS_OPEN(menuIsOpen) => {...state, menuIsOpen}
    | TOGGLE_MENU_IS_OPEN => {...state, menuIsOpen: !state.menuIsOpen}
    };
  };

  let (state, dispatch) = React.useReducer(reducer, initialState);

  let closeMenu = () => dispatch(SET_MENU_IS_OPEN(false));
  let handleValueChange = Belt.Option.getWithDefault(onChange, ignore);
  let handleCountryChange = country => {
    handleValueChange(country##value);
    closeMenu();
  };

  // Fetch countries
  React.useEffect1(
    () => {
      Js.Promise.(
        CountriesAPI.getCountries()
        |> then_(response => dispatch(SET_COUNTRIES(response)) |> resolve)
        |> catch(e => {
             let error = CountriesAPI.handlePromiseFailure(e);
             let errorMessage =
               "Error fetching countries: "
               ++ Belt.Option.getWithDefault(error, "");
             Js.log(errorMessage);
             dispatch(SET_COUNTRIES([||]));
             resolve();
           })
        |> ignore
      );
      None;
    },
    [||],
  );

  let selectedCountry = {
    let defaultCountry = {
      CountriesAPI.value: "Select a country",
      label: "Select a country",
    };

    switch (country) {
    | None => defaultCountry
    | Some(value) =>
      let foundCountry =
        state.countries
        |> Js.Array.find(country => country.CountriesAPI.value === value);

      Belt.Option.getWithDefault(foundCountry, defaultCountry);
    };
  };

  <div className={combineClassNames("country-select", className)}>
    <button
      className="select-country" onClick={_ => dispatch(TOGGLE_MENU_IS_OPEN)}>
      {selectedCountry.label |> ReasonReact.string}
      <img height="16px" src=caret />
    </button>
    <ReactSelect
      options={state.countries |> Js.Array.map(countryToOption)}
      components={
        "MenuList": CountrySelect_Renderers.menuRenderer,
        "Option": CountrySelect_Renderers.countryOptionRenderer,
        "DropdownIndicator": _ => React.null,
      }
      value={selectedCountry |> countryToOption}
      onChange=handleCountryChange
      menuIsOpen={state.menuIsOpen}
      onBlur={_ => closeMenu()}
    />
  </div>;
};