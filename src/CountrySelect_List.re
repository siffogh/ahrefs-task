type rowProps = {
  .
  index: int,
  key: string,
  style: ReactDOMRe.style,
};

[@bs.module "react-virtualized/"] [@react.component]
external make:
  (
    ~width: int,
    ~height: int,
    ~rowCount: int,
    ~rowHeight: int,
    ~scrollToIndex: int,
    ~rowRenderer: Js.t(rowProps) => React.element,
    ~className: string
  ) =>
  React.element =
  "List";