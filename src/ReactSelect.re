type reactSelectOption = {
  .
  label: string,
  value: string,
};

module OriginalReactSelect = {
  [@bs.module "react-select"] [@react.component]
  external make:
    (
      ~autoFocus: bool,
      ~components: Js.t('components),
      ~controlShouldRenderValue: bool,
      ~hideSelectedOptions: bool,
      ~menuIsOpen: bool,
      ~onBlur: ReactEvent.Form.t => unit,
      ~onChange: Js.t(reactSelectOption) => unit,
      ~options: array(Js.t(reactSelectOption)),
      ~tabSelectsValue: bool,
      ~value: Js.t(reactSelectOption)
    ) =>
    React.element =
    "default";
};

module OptionComponent = {
  [@bs.module "react-select"] [@bs.scope "children"] [@react.component]
  external make:
    (
      ~cx: Js.t('cx),
      ~clearValue: Js.t('clearValue),
      ~getStyles: Js.t('getStyles),
      ~getValue: Js.t('d),
      ~hasValue: bool,
      ~isMulti: bool,
      ~isRtl: bool,
      ~options: array(Js.t(reactSelectOption)),
      ~selectOption: Js.t(reactSelectOption) => unit,
      ~setValue: Js.t('setValue),
      ~selectProps: Js.t('selectProps),
      ~theme: Js.t('theme),
      ~innerProps: Js.t('innerProps),
      ~data: Js.t('data),
      ~isDisabled: bool,
      ~isSelected: bool,
      ~label: string,
      ~type_: string,
      ~value: string,
      ~innerRef: Js.t('innerRef),
      ~isFocused: bool,
      ~children: React.element
    ) =>
    React.element =
    "Option";
};

[@react.component]
let make = (~options, ~components, ~value, ~onChange, ~menuIsOpen, ~onBlur) => {
  !menuIsOpen
    ? React.null
    : <OriginalReactSelect
        options
        components
        value
        onChange
        autoFocus=true
        controlShouldRenderValue=false
        hideSelectedOptions=false
        tabSelectsValue=false
        menuIsOpen=true
        onBlur
      />;
};