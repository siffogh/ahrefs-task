module ReactSelect_Option = {
  [@bs.module "react-select"]
  [@bs.scope "components"]
  [@bs.val]
  [@react.component]
  external make:
    (
      ~cx: Js.t('a),
      ~clearValue: Js.t('b),
      ~getStyles: Js.t('c),
      ~getValue: Js.t('d),
      ~hasValue: bool,
      ~isMulti: bool,
      ~isRtl: bool,
      ~options: array(Js.t('o)),
      ~selectOption: Js.t('e),
      ~setValue: Js.t('f),
      ~selectProps: Js.t('g),
      ~theme: Js.t('h),
      ~innerProps: Js.t('i),
      ~data: Js.t('j),
      ~isDisabled: bool,
      ~isSelected: bool,
      ~label: string,
      ~type_: string,
      ~value: string,
      ~innerRef: Js.t('k),
      ~isFocused: bool,
      ~children: React.element
    ) =>
    React.element =
    "Option";
};

[@react.component]
let make = (~props, ~children) => {
  <ReactSelect_Option
    cx={
      props##cx;
    }
    clearValue={
      props##clearValue;
    }
    getStyles={
      props##getStyles;
    }
    getValue={
      props##getValue;
    }
    hasValue={
      props##hasValue;
    }
    isMulti={
      props##isMulti;
    }
    isRtl={
      props##isRtl;
    }
    options={
      props##options;
    }
    selectOption={
      props##selectOption;
    }
    setValue={
      props##setValue;
    }
    selectProps={
      props##selectProps;
    }
    theme={
      props##theme;
    }
    innerProps={
      props##innerProps;
    }
    data={
      props##data;
    }
    isDisabled={
      props##isDisabled;
    }
    isSelected={
      props##isSelected;
    }
    label={
      props##label;
    }
    type_={
      props##type_;
    }
    value={
      props##value;
    }
    innerRef={
      props##innerRef;
    }
    isFocused={
      props##isFocused;
    }>
    children
  </ReactSelect_Option>;
};