# Ahrefs Task | CountrySelect

## Requirements
- [x] Implemented bindings from scratch.
- [x] Countries fetched during runtime from the [countries api](https://gist.githubusercontent.com/rusty-key/659db3f4566df459bd59c8a53dc9f71f/raw/4127f9550ef063121c564025f6d27dceeb279623/counties.json).
- [x] Countries are rendered according to the [provided design](https://www.dropbox.com/s/94u8vilwquumgyq/country-selector.sketch?dl=0).
- [x] The search filter is internal. It filters countries by name without case sensitivity.
- [x] It supports keyboard (user can open, navigate and select options with keyboard).
- [ ] Select renders only visible options; it is not slow on opening. (👉 check the section below)


### About the last requirement
In order to implement the last requirement, I thought of the following options:
- **Option 1:** using [react-virtualized-select](https://github.com/bvaughn/react-virtualized-select).
- **Option 2:** using the `menuRenderer` prop of `react-select` with `react-virtualized/list`.
- **Option 3:** implementing the windowing manually based on the `onScroll` event listener and the `event.target.scrollTop` and `event.target.clientHeight` properties.

Unfortunately, due to time limit I had to opt out of this feature 😞.

## Run Project
```sh
npm install
npm start

# in another tab
npm run server
```

## Project content

### `App.js`
Contains the `App` component to test `CountrySelect`.

### `CountrySelect.js`
The source code for the `CountrySelect` component. The props are as required:

| Prop      | Type                   | Required |
|-----------|------------------------|----------|
| className | string                 |          |
| country   | option(string)         | required |
| onChange  | string => unit         |          |

### `ReactSelect`
Bindings to `react-select`.